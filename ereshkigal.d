module ereshkigal;
import page;
import std.stdio;

int main(string[] args) {
	if (args.length == 3) {
		dir root = readdir(std.path.baseName(args[1]), args[2]);
		root.descend();
		return 0;
	} else {
		writefln("\x1b[1musage:\x1b[0m %s input output", args[0]);
		return 2;
	}
}