module page;
static import parser, lexer;
import std.stdio;
import std.conv;
static import std.path;

static import std.file;
string page(string title, int hue, string content) {
	return "<!doctype html>
<html>
<head>
	<title>"~title~"</title>
	<meta charset=\"utf-8\">
	<link href=\"http://fonts.googleapis.com/css?family=EB+Garamond\" rel=\"stylesheet\" type=\"text/css\">
	<style type=\"text/css\">
		body {
			background: hsl("~to!string(hue)~",84%,12%);
			color: hsl("~to!string(hue)~",100%,89%);
			max-width: 30em;
			text-shadow: 0px 0px 30pt hsl("~to!string(hue)~",35%,61%);
			margin: auto;
			text-align: justify;
			font-family: \"GaramondNo8\", \"EB Garamond\", serif;
			font-size: 14pt;
		}
		a[href] {
			color: hsl("~to!string(hue)~",100%,89%)
		}		
	</style>
</head>
<body>\n"~content~"\n</body>
</html>";
}

struct story {
	size_t chapter;
	string filename;
	string title;
	string author;
	string html;
	string render() {
		parser.filename=filename; // i am so sorry
		return "<h1>"~title~"</h1>\n"~parser.parse(lexer.lex(html)).render();
	}
}
string sanitize(string s) { // the right way to write this won't work
	string buf;				// because math is broken. idk. i'm fucking
	for (size_t i = 0; i<s.length; ++i) { // tired and i don't give a shit
		if (s[i] >= 'A' && s[i] <= 'Z') buf~=cast(char)s[i]+0x20; //tolower
		else if (s[i] == ' ') buf~='-';
		else if (s[i] < ',') continue;
		else buf~=s[i];
	}
	return to!string(buf);
}
struct dir {
	string name;
	string path;
	dir[] dirs;
	story[] stories;
	int hue;
	void descend() {
		string cd = std.file.getcwd();
		std.file.mkdir(path);
		std.file.chdir(path);
		std.file.write("index.html", page(name, hue, listing()));
		foreach (story s; stories)
			std.file.write(s.filename, page(s.title, hue, s.render()));
		
		foreach (dir d; dirs)
			d.descend();
		std.file.chdir(cd);
	}
	string listing() {
		string l = "<h1>"~name~"</h1>\n";
		if (dirs!=null) {
			l~="<ul>\n";
			foreach (dir d; dirs) {
				l ~= "\t<li><b><a href=\""~d.path~"/index.html\">"~d.name~"</a></b></li>\n";
			}
			l~="</ul>\n";
		}
		if (stories!=null) {
			l~="<ol>\n";
			foreach (story s; stories) {
				l ~= "\t<li><a href=\""~s.filename~"\">"~s.title~"</a></li>\n";
			}
		}
		return l~"</ol>\n";
	}
}

void metadata(ref story s, string input) {
	void die(string e) {
		writefln("\x1b[36;1mmetadata error:\x1b[0;1m %s", e);
		std.c.stdlib.exit(4);
	}
	bool st = true;
	bool read_buf = false;
	string field = "";
	string buf = "";
	for (size_t i = 0; i<input.length; ++i) {
		if (st && input[i] == '-') { s.html = input[i+1..input.length]; return; }
		if (input[i] == '\n') {
			if (field!="") {
				switch (field) {
					case "title": s.title = buf; break;
					case "author": s.author = buf; break;
					case "chapter": s.chapter = to!uint(buf); break;
					default: die("unrecognized field \""~field~"\""); break;
				}
				field="";
				buf="";
			}
			st = true;
		} else { 
			if (read_buf) {
				buf~=input[i];
			} else {
				if (input[i]==':') {
					if (field=="") die("unexpected ':'");
					read_buf = true;
					while (input[i+1] == ' ') ++i;
				}
				else field~=input[i];
			}
			st = false;
		}
	}
	die("unterminated metadata");
}

void dirlist(ref dir d) {
	auto l = std.file.getcwd();
	std.file.chdir(d.name);
	foreach (string file; std.file.dirEntries(".",std.file.SpanMode.shallow)) {
		if (std.file.isDir(file)) {
			dir nd = {
				name: std.path.baseName(file),
				path: sanitize(file),
				hue: d.hue,
				dirs: [],
				stories: [],
			};
			dirlist(nd);
			d.dirs ~= nd;
		} else {
			string text = std.file.readText(file);
			story s = {
				chapter: 0,
				title: null,
				filename: std.path.stripExtension(file)~".html",
				author: null,
				html: null,
			};
			metadata(s, text);
			d.stories ~= s;
		}
	}
	
	std.file.chdir(l);
}
dir readdir(string n, string p) {
	dir root = {
		name: n,
		path: p,
		hue: 328,
		dirs: [],
		stories: []
	};
	dirlist(root);
	return root;
}