module lexer;
import std.stdio;

struct token {
	enum kind_t {block, newpar, escape, lbrac, rbrac, /* haaaaaack */ eof};
	kind_t kind;
	string buf;
	void dump() {
		write("- ");
		final switch (kind) {
			case kind_t.block: writefln("\x1b[1mBLOCK\x1b[0m: \n\"%s\"", buf); break;
			case kind_t.newpar: writeln("\x1b[1mNEWPAR\x1b[0m"); break;
			case kind_t.escape: writefln("\x1b[1mESCAPE\x1b[0m: \n\"%s\"", buf); break;
			case kind_t.lbrac: writeln("\x1b[1m[\x1b[0m"); break;
			case kind_t.rbrac: writeln("\x1b[1m]\x1b[0m"); break;
			case kind_t.eof: writeln("\x1b[1;31mEOF\x1b[0m"); break;
		}
	}
}

token[] lex (string str) {
	void die(string e, size_t curs) {
		size_t bgn = curs;
		while (bgn > 0 && str[bgn]!='\n') --bgn;
		size_t end = ++bgn;
		while (end < str.length && str[end]!='\n') ++end;
		writefln("\x1b[35;1mparse:\x1b[0;1m %s\x1b[0m",e);
		writefln(" \x1b[35m:\x1b[0m %s\x1b[31m%s\x1b[0m", str[bgn..curs], str[curs..end]);
		std.c.stdlib.exit(1);
	}
	token[] tb;
	token ct;
	ct.kind=token.kind_t.eof;
	ct.buf = null;
	void finalize() {
		tb ~= ct;
		ct.buf=null;
		ct.kind = token.kind_t.eof;
	}
	bool isws(char c) {
		return (c==' ') || (c=='\n') || (c=='\t');
	}
	bool isid(char c) {
		return !isws(c) && c!='[' && c != ']' && c!='.' && c!=':';
	}
	void buf(char c) {
		if (ct.buf == null) ct.buf = [c];
		else ct.buf ~= c;
	}
	void wbuf(string c) {
		if (ct.buf == null) ct.buf = c;
		else ct.buf ~= c;
	}
	for (size_t i = 0; i<str.length; ++i) {
		char c = str[i];
		switch (ct.kind) {
			case token.kind_t.eof:
	 			if (c=='\\' && isid(str[i+1])) { ct.kind=token.kind_t.escape; continue; }
	 			if (c=='[') { ct.kind=token.kind_t.lbrac; finalize(); continue; }
	 			if (c==']') { ct.kind=token.kind_t.rbrac; finalize(); continue; }
	 			if (c=='\\' && !isid(str[i+1])) { ++i; c=str[i]; }
	 			ct.buf = [c];
	 			ct.kind = token.kind_t.block;
			break;
			case token.kind_t.block:
				if (c=='\n' && (i+1<str.length) && str[i+1] == '\n') {
					++i;
					finalize();
					tb~=token(token.kind_t.newpar, null);
				} else if (c=='[') {
					finalize();
					ct.kind=token.kind_t.lbrac;
					finalize();
				} else if (c==']') {
					finalize();
					ct.kind=token.kind_t.rbrac;
					finalize();
				} else if (c=='\\' && isid(str[i+1])) { //cheating, but
					finalize();
					ct.kind = token.kind_t.escape;
					continue;
				} else if (c=='\\' && !isid(str[i+1]) ) {
					buf(str[i+1]); ++i;
				} else if (c=='`' && str[i+1]=='`') {
					wbuf("“"); ++i;
				} else if (c=='\'' && str[i+1]=='\'') {
					wbuf("”"); ++i;
				} else {
					buf(c);
				}
			break;
			case token.kind_t.escape:
				if (c=='\\') die("invalid char in escape", i);
				buf(c);
				if (i+1 <= str.length) {
					if (str[i+1] == ';') { ++i; finalize(); }
					if (!isid(str[i+1])) { finalize(); }
				} else { finalize(); }
			break;
			default:break;
		}
	}
	if (ct.kind != token.kind_t.eof) finalize(); // haack
	return tb;
}