module parser;
static import lexer;
import std.stdio;
static import std.c.stdlib;
import std.conv;
class node {
	abstract string render();
	void dump(){ writefln("- %s", this);}
}

void die(string e) {
	writefln("\x1b[31;1mfatal:\x1b[0m %s",e);
	std.c.stdlib.exit(1);
}

string filename = null; // i am an awful garbage person

string plistr(node[] ps){
	string n = "";
	foreach(node p; ps) n~=p.render();
	return n;
} 

class tag : node {
	string id;
	node[][] params;
	override string render() {
		if (id == "i" && params && params.length == 1) {
			return "<em>"~plistr(params[0])~"</em>";
		} else if (id == "b" && params && params.length == 1) {
			return "<strong>"~plistr(params[0])~"</strong>";
		} else if (id == "time") {
			return "(current time)";
		} else if (id == "ln-ext" && params && params.length == 2) {
			return "<a href=\""~plistr(params[1])~"\">"~plistr(params[0])~"</a>";
		} else if (id == "ln-idx" && params && params.length == 1) {
			return "<a href=\"./\">"~plistr(params[0])~"</a>";
		} else if (id == "ln-up" && params && params.length == 1) {
			return "<a href=\"../\">"~plistr(params[0])~"</a>";
		} else {
			writeln(plistr(params[0]));
			die("invalid tag "~id~" with "~to!string(params.length)~" params in "~filename);
			assert(0);
		}
	}
	override void dump() {
		writefln("TAG %s [", id);
		//foreach(node n; params) n.dump();
		writeln("]\n");
	}
	this(string str) {id=str; params = null;}
}

class text : node {
	string text;
	override string render() {return text;}
	this(string str) {text=str;}
}

class doc : node {
	node[] nodes;
	this() { nodes = []; }
	override string render() {
		string v = "";
		foreach(node n; nodes) v~=n.render();
		return v~"</p>";
	}
}

size_t match(T...)(lexer.token[] list, T tokens) {
	size_t i = 0;
	foreach(t; tokens) {
		if (list[i].kind != t) return 0;
		++i;
	}
	return i;
}

doc parse(lexer.token[] t) {
	doc d = new doc;
	_parse(t, d.nodes);
	return d;
}

size_t _parse(lexer.token[] t, ref node[] d) {
	size_t i = 0;
	text pbreak = new text("</p>\n\n<p>"); //HAX
	with (lexer.token.kind_t) while (i<t.length) {
		auto cs = t[i..t.length];
		if (size_t l = match(cs, block)) { i+=l;
			d ~= new text(cs[0].buf);
		} else if (size_t l = match(cs, escape)) { i+=l;
			auto n = new tag(cs[0].buf);
			writefln("creating new node %s", cs[0].buf);
			n.params=[];
			size_t z;
			while (i<t.length && t[i].kind==lbrac) {
				n.params.length++;
				i+=_parse(t[i+1..t.length], n.params[n.params.length-1])+1;
			}
			d ~= n;
		} else if (size_t l = match(cs, rbrac)) { i+=l;
			writeln("leaving tag");
			return i;
		} else if (size_t l = match(cs, newpar)) { i+=l;
			d ~= pbreak;
		} else {
			die("parse failure!");
		}
	}
	return i;
}